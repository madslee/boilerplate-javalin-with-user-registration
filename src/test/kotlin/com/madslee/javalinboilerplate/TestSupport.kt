package com.madslee.javalinboilerplate

import com.madslee.javalinboilerplate.persistence.HikariPool

class TestSupport {

    companion object {

        fun loadTestEnvironmentProperties() {
            AppProperties.readProperties(
                "src/test/resources/application.properties",
                "src/test/resources/email.properties",
                "src/main/resources/message.properties")
        }

        fun setDataPool() = HikariPool.setUpDatabaseAndConnection(
                AppProperties.properties.getProperty("database.url"),
                AppProperties.properties.getProperty("database.username"),
                AppProperties.properties.getProperty("database.password"),
                AppProperties.properties.getProperty("database.driver"))
    }
}

