package com.madslee.javalinboilerplate.services

import com.madslee.javalinboilerplate.TestSupport
import com.madslee.javalinboilerplate.utils.exceptions.AuthenticationException
import com.madslee.javalinboilerplate.models.ActivationKey
import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.persistence.repositories.UserRepository
import com.madslee.javalinboilerplate.persistence.repositories.UserRepositoryImpl
import com.madslee.javalinboilerplate.utils.EmailService
import com.madslee.javalinboilerplate.utils.exceptions.ValidationException
import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mindrot.jbcrypt.BCrypt
import java.lang.NullPointerException

internal class UserServiceImplTest {

    private val userRepositoryMock: UserRepository = mock()
    private val userRepository: UserRepository
    private val emailService: EmailService = mock()
    private val serviceAllMocks: UserServiceImpl
    private val serviceWithRepository: UserServiceImpl

    init {
        TestSupport.loadTestEnvironmentProperties()
        TestSupport.setDataPool()

        userRepository = UserRepositoryImpl()
        serviceAllMocks = UserServiceImpl(userRepositoryMock, emailService)
        serviceWithRepository = UserServiceImpl(userRepository, emailService)
    }

    @Test
    fun saveNewUser() {
        val password = "password"
        val user = User(null, "email", password, false)
        val activationKey = ActivationKey(null, user, "12121")

        whenever(userRepositoryMock.saveUser(user)).thenReturn(user)
        whenever(userRepositoryMock.saveUserActivationKey(activationKey)).then { }
        whenever(emailService.sendEmail(any(), any(), any())).then { }

        // Check that password is hashed
        serviceAllMocks.saveNewUser(user)
        assertNotEquals(user.password, password)

        // Check that mail is sent to user's email address
        argumentCaptor<String>().apply {
            verify(emailService, times(1)).sendEmail(capture(), any(), any())
            assertEquals(user.email, firstValue)
        }
    }

    @Test
    fun getUserIdForLoginCredentials() {
        val email = "email@email.com"
        val password = "password"
        val user = User(123, email, BCrypt.hashpw(password, BCrypt.gensalt()), true)


        val savedUser = userRepository.saveUser(user)
        val userId = serviceWithRepository.getUserIdForLoginCredentials(email, password)

        assertNotNull(userId)
        assertEquals(savedUser.getId(), userId)
    }

    @Test
    fun getUserIdForLoginCredentialsUnconfirmedUser() {
        val email = "email@otheremail.com"
        val password = "password"
        val user = User(123, email, BCrypt.hashpw(password, BCrypt.gensalt()), false)

        val savedUser = userRepository.saveUser(user)
        assertThrows(ValidationException::class.java) {
            val userId = serviceWithRepository.getUserIdForLoginCredentials(email, password)
        }
    }

    @Test
    fun deleteNewUserWhenEmailFails() {
        whenever(emailService.sendEmail(any(), any(), any())).thenThrow(RuntimeException::class.java)

        val email = "invalid@email.com"
        val user = User(null, email, "password", false)

        assertThrows(ValidationException::class.java) {
            serviceWithRepository.saveNewUser(user)
        }
        assertNull(userRepository.getUser(email))
    }

    @Test
    fun deleteUser() {
        val userId = 123
        val password = "password"

        val user = User(userId, "email", BCrypt.hashpw(password, BCrypt.gensalt()), true)
        userRepository.saveUser(user)

        val userWithCleartextPassword = User(userId, "email", password, true)

        serviceWithRepository.deleteUserAccount(userWithCleartextPassword)
        assertThrows(NullPointerException::class.java) {
            userRepository.getUser(userId)
        }
    }

    @Test
    fun getUserIdForLoginCredentialsBadCredentials() {
        val email = "otheremail@email.com"
        val password = "password"
        val user = User(123, email, BCrypt.hashpw(password, BCrypt.gensalt()), true)

        val savedUser = userRepository.saveUser(user)

        assertThrows(AuthenticationException::class.java) {
            serviceWithRepository.getUserIdForLoginCredentials(email, "badPassword")
        }
    }
}