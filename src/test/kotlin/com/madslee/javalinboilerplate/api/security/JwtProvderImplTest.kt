package com.madslee.javalinboilerplate.api.security

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class JwtProvderImplTest {

    val jwtProvider =
        JwtProvderImpl("secret", "appn", 36000)

    @Test
    fun createAndVerifyToken() {
        val userId = 3
        val jwt = jwtProvider.generateJwt(userId)

        assertTrue(jwt.isNotEmpty())
        assertTrue(jwt.length >= 40)

        val userIdFromJwt = jwtProvider.getUserIdFromJwt(jwt)
        assertEquals(userId, userIdFromJwt)
    }

}