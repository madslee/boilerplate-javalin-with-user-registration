package com.madslee.javalinboilerplate.persistence.repositories

import com.madslee.javalinboilerplate.TestSupport
import com.madslee.javalinboilerplate.models.ActivationKey
import com.madslee.javalinboilerplate.models.User
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class UserRepositoryImplTest {

    private lateinit var repo: UserRepositoryImpl

    @BeforeEach
    fun setup() {
        TestSupport.loadTestEnvironmentProperties()
        repo = UserRepositoryImpl()
    }

    @Test
    fun insertUserAndTestThatIdIsGiven() {
        val savedUser = repo.saveUser(User(null, "username", "userEmail", false))
        assertNotNull(savedUser.getId())

        val fetchedSavedUser = repo.getUser(savedUser.getId()!!)
        assertEquals(savedUser.email, fetchedSavedUser.email)

        // Check that only one user
        assertEquals(1, repo.getAllUsers().size)

        // Find user by email
        val fetchedSavedUserByEmail = repo.getUser(savedUser.email)
        assertEquals(fetchedSavedUser.getId(), fetchedSavedUserByEmail!!.getId()
        )
    }

    @Test
    fun saveActivationKey() {
        val savedUser = repo.saveUser(User(null, "name", "otherEmail", false))
        val activationKey = ActivationKey(null, savedUser, "sds")

        repo.saveUserActivationKey(activationKey)
    }
}