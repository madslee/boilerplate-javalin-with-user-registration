package com.madslee.javalinboilerplate.utils

import com.madslee.javalinboilerplate.AppProperties
import com.madslee.javalinboilerplate.TestSupport
import com.madslee.javalinboilerplate.utils.EmailServiceImpl
import org.junit.jupiter.api.Test

internal class EmailServiceImplTest {

    private var emailSender: EmailServiceImpl

    init {
        TestSupport.loadTestEnvironmentProperties()
        emailSender =
            EmailServiceImpl(AppProperties.emailProperties)
    }

    @Test
    fun sendTestEmail() {
        // Success if no errors thrown
        emailSender.sendEmail("madsgiil.no@gmail.com", "from javalin", "test from javalin")
    }

}