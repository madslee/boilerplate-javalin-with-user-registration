package com.madslee.javalinboilerplate.utils

import com.madslee.javalinboilerplate.utils.EmailService
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

class EmailServiceImpl(private val emailProperties: Properties) :
    EmailService {

    override fun sendEmail(recipientAddress: String, subject: String, body: String) {
        val message = MimeMessage(getSession())
        message.setFrom(InternetAddress(emailProperties.getProperty("mail.address")))
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientAddress))
        message.subject = subject

        val mimeBody = MimeBodyPart()
        mimeBody.setContent(body, "text/html")

        val multipart = MimeMultipart()
        multipart.addBodyPart(mimeBody)

        message.setContent(multipart)
        Transport.send(message)
    }

    private fun getSession(): Session {
        return Session.getInstance(emailProperties, object : Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication {
                return PasswordAuthentication(
                    emailProperties.getProperty("mail.username"),
                    emailProperties.getProperty("mail.password")
                )
            }
        })
    }

}