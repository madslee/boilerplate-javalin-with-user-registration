package com.madslee.javalinboilerplate.utils.exceptions


class ValidationException(val messageKey: String): Exception()
class AuthenticationException(val messageKey: String): Exception()