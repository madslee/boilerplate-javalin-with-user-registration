package com.madslee.javalinboilerplate.utils

import org.slf4j.LoggerFactory

class EmailServiceMock: EmailService {

    val logger = LoggerFactory.getLogger(EmailServiceMock::class.java)

    override fun sendEmail(recipientAddress: String, subject: String, body: String) {
        logger.info("Mock sending email with subject: $subject and body: $body to $recipientAddress")
    }
}