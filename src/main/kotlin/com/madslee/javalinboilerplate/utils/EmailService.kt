package com.madslee.javalinboilerplate.utils

interface EmailService {
    fun sendEmail(recipientAddress: String, subject: String, body: String)
}