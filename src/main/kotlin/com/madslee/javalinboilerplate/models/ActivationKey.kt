package com.madslee.javalinboilerplate.models

data class ActivationKey (
    private val id: Int?,
    val user: User,
    val key: String
) {
    fun getId() = id
}