package com.madslee.javalinboilerplate.models

import com.fasterxml.jackson.annotation.JsonProperty

data class User (
    private val id: Int?,
    val email: String,
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var password: String,
    var accountConfirmed: Boolean = false
) {
    fun getId() = id
}