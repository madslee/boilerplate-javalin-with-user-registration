package com.madslee.javalinboilerplate.api.security

import com.auth0.jwt.exceptions.JWTVerificationException


interface JwtProvider {
    fun generateJwt(userId: Int): String

    @Throws(JWTVerificationException::class)
    fun getUserIdFromJwt(jwt: String): Int
}