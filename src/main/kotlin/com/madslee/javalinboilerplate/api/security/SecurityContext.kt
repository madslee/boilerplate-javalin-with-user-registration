package com.madslee.javalinboilerplate.api.security

class SecurityContext {

    companion object {
        private var USER_ID: Int? = null

        fun setUserId(userId: Int) {
            USER_ID = userId
        }

        fun getUserId() = USER_ID
    }
}