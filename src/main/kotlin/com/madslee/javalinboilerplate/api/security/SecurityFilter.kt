package com.madslee.javalinboilerplate.api.security

import io.javalin.http.Context
import io.javalin.http.UnauthorizedResponse

class SecurityFilter {

    companion object {

        fun doFilter(nonProtectedUrls: List<String>, context: Context, jwtProvider: JwtProvider) {
            val requestUrl = context.url()

            val nonProtectedUrl = nonProtectedUrls
                .stream()
                .anyMatch { x -> requestUrl.contains(x) }

            if (!nonProtectedUrl) {
                val jwt = context.req.getHeader("Authorization")
                val userId: Int
                try {
                    userId = jwtProvider.getUserIdFromJwt(jwt)
                } catch (e: Exception) {
                    throw UnauthorizedResponse()
                }
                SecurityContext.setUserId(userId)
            }
        }
    }
}