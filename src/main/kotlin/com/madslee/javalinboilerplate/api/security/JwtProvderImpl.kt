package com.madslee.javalinboilerplate.api.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import java.util.*

class JwtProvderImpl(private val secret: String, private val tokenIssuer: String, private val validityDurationMilliseconds: Long): JwtProvider {

    private val algorithm = Algorithm.HMAC256(secret)
    private val userIdClaim = "userId"

    override fun generateJwt(userId: Int): String {
        return JWT.create()
            .withIssuer(tokenIssuer)
            .withClaim(userIdClaim, userId)
            .withExpiresAt(Date(Date().time + validityDurationMilliseconds))
            .sign(algorithm)
    }

    override fun getUserIdFromJwt(jwt: String): Int {
        val verifier = JWT.require(algorithm)
            .withIssuer(tokenIssuer)
            .build()

        val jwtDecoded = verifier.verify(jwt)
        return jwtDecoded.getClaim(userIdClaim).asInt()
    }
}