package com.madslee.javalinboilerplate.api

import com.auth0.jwt.exceptions.JWTVerificationException
import com.madslee.javalinboilerplate.AppProperties
import com.madslee.javalinboilerplate.api.controllers.ExampleProtectedUrlController
import com.madslee.javalinboilerplate.api.controllers.UserController
import com.madslee.javalinboilerplate.api.security.JwtProvider
import com.madslee.javalinboilerplate.api.security.SecurityFilter
import com.madslee.javalinboilerplate.utils.exceptions.AuthenticationException
import com.madslee.javalinboilerplate.utils.exceptions.ValidationException
import com.madslee.javalinboilerplate.services.UserService
import io.javalin.Javalin
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.HttpResponseException
import io.javalin.http.UnauthorizedResponse
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.LocalDateTime

class RestApi(private val app: Javalin): KoinComponent {

    private val userService: UserService by inject()
    private val jwtProvider: JwtProvider by inject()

    init {

        // Add filter
        app.before { ctx ->
            val nonProtectedUrls = listOf("${app.config.contextPath}/${AppProperties.properties.getProperty("security.open.routes")}")
            SecurityFilter.doFilter(nonProtectedUrls, ctx, jwtProvider)
        }

        // Exception handling
        app.exception(ValidationException::class.java) { e, ctx ->
            setErrorResponseToContext(ctx, e.messageKey, 400)
        }

        app.exception(AuthenticationException::class.java) { e, ctx ->
            setErrorResponseToContext(ctx, e.messageKey, 403)
        }

        app.exception(JWTVerificationException::class.java) { e, ctx -> {
            setErrorResponseToContext(ctx, "", 403)
        }}

        app.exception(Exception::class.java) { e, ctx ->
            setErrorResponseToContext(ctx, "Unknown exception", 500)
        }

        // Start controllers
        UserController(app, userService, jwtProvider)
        ExampleProtectedUrlController(app)
    }


    private fun getErrorMessage(messageKey: String) = AppProperties.messageProperties.getProperty(messageKey)

    private fun setErrorResponseToContext(context: Context, messageKey: String, statusCode: Int) {
        context.status(statusCode)
        context.json(ErrorResponse(getErrorMessage(messageKey), statusCode))
    }

    private data class ErrorResponse(
        val message: String,
        val status: Int,
        val timeStamp: String = LocalDateTime.now().toString()
    )
}