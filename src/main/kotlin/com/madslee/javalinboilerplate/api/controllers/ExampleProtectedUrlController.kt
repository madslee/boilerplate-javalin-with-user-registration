package com.madslee.javalinboilerplate.api.controllers

import com.madslee.javalinboilerplate.api.security.SecurityContext
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.http.Context

class ExampleProtectedUrlController(private val app: Javalin) {

    init {
        app.routes {
            ApiBuilder.path("example") {
                ApiBuilder.get { ctx: Context ->
                    val userId = SecurityContext.getUserId()
                    ctx.result("This endpoint is protected, your user ID is $userId")
                }
            }
        }
    }

}