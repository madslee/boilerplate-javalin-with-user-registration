package com.madslee.javalinboilerplate.api.controllers

import com.madslee.javalinboilerplate.api.security.JwtProvider
import com.madslee.javalinboilerplate.utils.exceptions.ValidationException
import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.services.UserService
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.http.Context

/**
 * Controller with no protected endpoints, eg. all endpoints must
 * ensure their own authentication and authorization if necessary
 */
class UserController(private val app: Javalin, private val userService: UserService, private val jwtProvider: JwtProvider) {

    init {
        app.routes {
            path("users") {

                post { context: Context ->
                    val user = context.bodyAsClass(User::class.java)
                    val savedUser = userService.saveNewUser(user)
                    context.json(savedUser)
                }

                delete { context: Context ->
                    val user = context.bodyAsClass(User::class.java)
                    userService.deleteUserAccount(user)
                }

                patch(":id/:confirmationcode") { context ->
                    val confirmationCode = context.pathParam("confirmationcode")
                    val id: Int

                    try {
                        id = context.pathParam("id").toInt()
                    } catch (e: Exception) {
                        throw ValidationException("id.not.number")
                    }
                    context.json(userService.confirmUserAccount(id, confirmationCode))
                }

                post("login") { context ->
                    val user = context.bodyAsClass(User::class.java)
                    val userId = userService.getUserIdForLoginCredentials(user.email, user.password)
                    val jwt = jwtProvider.generateJwt(userId)
                    context.result(jwt)
                }

                put("password:email") { context ->
                    val email = context.pathParam("email")
                    userService.resetForgottenPassword(email)
                }
            }
        }
    }
}