package com.madslee.javalinboilerplate.persistence

import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.models.ActivationKey
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object AppUsers: IntIdTable() {
    val email = varchar("email", 100).uniqueIndex()
    val password = varchar("password", 400)
    val accountConfirmed = bool("confirmed")
}

object ActivationKeys: IntIdTable() {
    val user = reference("app_user", AppUsers.id)
    val activationKey = varchar("activationKey", 100)
}

class AppUserEntity(id: EntityID<Int>): IntEntity(id) {
    companion object: EntityClass<Int, AppUserEntity>(AppUsers)

    fun getId() = id.value
    var email by AppUsers.email
    var password by AppUsers.password
    var accountConfirmed by AppUsers.accountConfirmed

    fun toUser() = User(id.value, email, password, accountConfirmed)
}

class ActivationKeyEntity(id: EntityID<Int>): IntEntity(id) {
    companion object: EntityClass<Int, ActivationKeyEntity>(ActivationKeys)

    fun getId() = id.value
    var user by AppUserEntity referencedOn ActivationKeys.user
    var key by ActivationKeys.activationKey

    fun toActivationKey() = ActivationKey(id.value, user.toUser(), key)
}