package com.madslee.javalinboilerplate.persistence

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

class HikariPool() {

    companion object {
        lateinit var dataSource: HikariDataSource

        fun setUpDatabaseAndConnection(url: String, username: String, password: String, driverClassName: String) {
            val config = HikariConfig()
            config.jdbcUrl = url
            config.username = username
            config.password = password
            config.driverClassName = driverClassName
            dataSource = HikariDataSource(config)

            Database.connect(dataSource)
            transaction {
                SchemaUtils.create (AppUsers, ActivationKeys)
                commit()
            }
        }
    }
}