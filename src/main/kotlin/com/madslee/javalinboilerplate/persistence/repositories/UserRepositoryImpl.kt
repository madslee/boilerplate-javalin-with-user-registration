package com.madslee.javalinboilerplate.persistence.repositories

import com.madslee.javalinboilerplate.models.ActivationKey
import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.persistence.*
import com.madslee.javalinboilerplate.persistence.ActivationKeys.user
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.streams.toList

class UserRepositoryImpl(): UserRepository {

    init {
        Database.connect(HikariPool.dataSource)
    }

    override fun saveUser(user: User): User {
        return transaction {
            AppUserEntity.new {
                email = user.email
                password = user.password
                accountConfirmed = user.accountConfirmed
            }
        }.toUser()
    }

    override fun updateUser(user: User) {
        transaction {
            val userEntity = AppUserEntity.findById(user.getId()!!)!!
            userEntity.email = user.email
            userEntity.password = user.password
            userEntity.accountConfirmed = user.accountConfirmed
        }
    }

    override fun getUser(userId: Int): User {
        return transaction {
            AppUserEntity.findById(userId)
        }!!.toUser()
    }

    override fun getUser(email: String): User? {
        return transaction {
            AppUserEntity.find { AppUsers.email eq email }.firstOrNull()
        }?.toUser()
    }

    override fun getAllUsers(): List<User> {
        return transaction { AppUserEntity.all().toList()
            .stream()
            .map { x -> x.toUser() }
            .toList() }
    }

    override fun deleteUser(userId: Int) {
        transaction {
            ActivationKeys.deleteWhere { ActivationKeys.user eq userId }
            AppUsers.deleteWhere { AppUsers.id eq  userId }
        }
    }

    override fun saveUserActivationKey(activationKey: ActivationKey) {
        return transaction {
            ActivationKeyEntity.new {
                user = AppUserEntity.findById(activationKey.user.getId()!!)!!
                key = activationKey.key
            }
        }
    }

    override fun getActivationKey(userId: Int): ActivationKey? {
        return transaction {
            ActivationKeyEntity.find { user eq userId }.firstOrNull()?.toActivationKey()
        }
    }

    override fun deleteActivationKey(activationKey: ActivationKey) {
        transaction {
            ActivationKeys.deleteWhere { ActivationKeys.user eq activationKey.user.getId()!! }
        }
    }
}
