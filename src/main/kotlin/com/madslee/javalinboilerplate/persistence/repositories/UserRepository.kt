package com.madslee.javalinboilerplate.persistence.repositories

import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.models.ActivationKey

interface UserRepository {
    fun saveUser(user: User): User
    fun updateUser(user: User)
    fun getUser(userId: Int): User
    fun getUser(email: String): User?
    fun getAllUsers(): List<User>
    fun deleteUser(userId: Int)
    fun saveUserActivationKey(activationKey: ActivationKey)
    fun getActivationKey(userId: Int): ActivationKey?
    fun deleteActivationKey(activationKey: ActivationKey)
}