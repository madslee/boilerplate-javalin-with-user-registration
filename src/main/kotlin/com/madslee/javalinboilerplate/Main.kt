package com.madslee.javalinboilerplate

import com.madslee.javalinboilerplate.api.RestApi
import com.madslee.javalinboilerplate.api.security.JwtProvderImpl
import com.madslee.javalinboilerplate.api.security.JwtProvider
import com.madslee.javalinboilerplate.utils.EmailServiceMock
import com.madslee.javalinboilerplate.persistence.HikariPool
import com.madslee.javalinboilerplate.persistence.repositories.UserRepository
import com.madslee.javalinboilerplate.persistence.repositories.UserRepositoryImpl
import com.madslee.javalinboilerplate.services.UserService
import com.madslee.javalinboilerplate.services.UserServiceImpl
import com.madslee.javalinboilerplate.utils.EmailService
import io.javalin.Javalin
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.slf4j.LoggerFactory

fun main(args: Array<String>) {

    val logger = LoggerFactory.getLogger("Application Main")

    // Set properties
    var devMode = false
    var applicationPropertiesFile = "src/main/resources/application.properties"
    var emailPropertiesFile = "src/main/resources/email.properties"
    val messagePropertiesFile = "src/main/resources/message.properties"

    if (args.isNotEmpty()) {
        val environment = args[0]

        if (environment == "devMode") {
            logger.info("Starting application in developer mode")
            devMode = true
            applicationPropertiesFile = "src/main/resources/devApplication.properties"
            emailPropertiesFile = "src/main/resources/devEmail.properties"
        } else {
            error("Unknown argument: $environment")
        }
    }
    AppProperties.readProperties(applicationPropertiesFile, emailPropertiesFile, messagePropertiesFile)

    // Build database and create a connection pool
    HikariPool.setUpDatabaseAndConnection(
        AppProperties.properties.getProperty("database.url"),
        AppProperties.properties.getProperty("database.username"),
        AppProperties.properties.getProperty("database.password"),
        AppProperties.properties.getProperty("database.driver"))

    // Set up dependency injection
    val koinModules = module {
        single { UserRepositoryImpl() as UserRepository }
        single { UserServiceImpl(get(), get()) as UserService }
        single { JwtProvderImpl(
            AppProperties.properties.getProperty("jwt.secret"),
            AppProperties.properties.getProperty("jwt.issuer"),
            AppProperties.properties.getProperty("jwt.validity").toLong()) as JwtProvider }
    }

    val koinDevModules = module(override = true) {
        single { EmailServiceMock() as EmailService }
    }

    startKoin {
        printLogger()
        if (devMode) {
            modules(koinModules, koinDevModules)
        } else {
            modules(koinModules)
        }
    }

    // Start Javalin and its Rest API
    val app = Javalin.create { config ->
        config.contextPath = "/api"
    }.start(8080)
    RestApi(app)

    logger.info("Successfully started application")
}
