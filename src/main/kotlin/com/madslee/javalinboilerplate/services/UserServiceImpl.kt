package com.madslee.javalinboilerplate.services

import com.madslee.javalinboilerplate.utils.exceptions.AuthenticationException
import com.madslee.javalinboilerplate.utils.exceptions.ValidationException
import com.madslee.javalinboilerplate.models.ActivationKey
import com.madslee.javalinboilerplate.models.User
import com.madslee.javalinboilerplate.persistence.repositories.UserRepository
import com.madslee.javalinboilerplate.utils.EmailService
import org.mindrot.jbcrypt.BCrypt
import java.lang.Exception

class UserServiceImpl(private val userRepository: UserRepository, private val emailService: EmailService): UserService {

    override fun saveNewUser(user: User): User {
        if (userRepository.getUser(user.email) != null) throw ValidationException("user.duplicate.email")

        user.password = BCrypt.hashpw(user.password, BCrypt.gensalt())
        val savedUser = userRepository.saveUser(user)

        val randomString = generateRandomString()
        val activationKey = ActivationKey(null, savedUser, randomString)
        userRepository.saveUserActivationKey(activationKey)

        try {
            emailService.sendEmail(savedUser.email, "Signed up on com.madslee.javalinboilerplate", "Your activation code: ${savedUser.getId()}/$randomString")
        } catch (e: Exception) {
            userRepository.deleteUser(savedUser.getId()!!)
            throw ValidationException("email.not.valid.signup")
        }

        return savedUser
    }

    override fun deleteUserAccount(user: User) {
        val confirmedIdOfAuthenticatedUser = getUserIdForLoginCredentials(user.email, user.password)
        userRepository.deleteUser(confirmedIdOfAuthenticatedUser)
    }

    override fun confirmUserAccount(userId: Int, confirmationCode: String): User {
        val user = getUser(userId)
        if (user.accountConfirmed) throw ValidationException("user.already.confirmed")

        val activationKey = userRepository.getActivationKey(userId) ?: throw ValidationException("user.no.activation.key.but.not.confirmed")
        if (activationKey.key != confirmationCode) throw ValidationException("user.activation.key.wrong")

        userRepository.deleteActivationKey(activationKey)
        user.accountConfirmed = true
        userRepository.updateUser(user)

        return user
    }

    @Throws(AuthenticationException::class)
    override fun getUserIdForLoginCredentials(email: String, password: String): Int {
        val user = getUser(email)

        if (!user.accountConfirmed) throw ValidationException("user.not.confirmed")

        if (BCrypt.checkpw(password, user.password)) {
            return user.getId()!!
        } else {
            throw AuthenticationException("user.not.authenticated")
        }
    }

    override fun resetForgottenPassword(email: String) {
        val user = getUser(email)

        val randomPassword = generateRandomString()
        user.password = BCrypt.hashpw(randomPassword, BCrypt.gensalt())

        emailService.sendEmail(user.email,"New password created","Change new password ASAP. New password: $randomPassword")
        userRepository.updateUser(user)
    }

    private fun getUser(id: Int): User {
        return try {
            userRepository.getUser(id)
        } catch (e: Exception) {
            throw ValidationException("user.id.not.valid")
        }
    }

    private fun getUser(email: String): User {
        return userRepository.getUser(email) ?: throw ValidationException("user.email.not.existing")
    }

    private fun generateRandomString(): String {
        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        val randomString = (1..18)
            .map {kotlin.random.Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("");
        return randomString
    }
}