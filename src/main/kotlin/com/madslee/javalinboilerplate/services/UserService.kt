package com.madslee.javalinboilerplate.services

import com.madslee.javalinboilerplate.utils.exceptions.AuthenticationException
import com.madslee.javalinboilerplate.models.User

interface UserService {
    fun saveNewUser(user: User): User

    fun deleteUserAccount(user: User)

    fun confirmUserAccount(userId: Int, confirmationCode: String): User

    @Throws(AuthenticationException::class)
    fun getUserIdForLoginCredentials(email: String, password: String): Int

    fun resetForgottenPassword(email: String)
}