package com.madslee.javalinboilerplate

import org.slf4j.LoggerFactory
import java.io.FileInputStream
import java.io.IOException
import java.util.*

class AppProperties() {

    companion object {
        private val appProperties = AppProperties()
        lateinit var properties: Properties
        lateinit var emailProperties: Properties
        lateinit var messageProperties: Properties

        fun readProperties(applicationPropertiesFilePath: String, emailPropertiesFilePath: String, messagePropertiesFilePath: String) {
            properties = appProperties.readPropertiesFile(applicationPropertiesFilePath)
            emailProperties = appProperties.readPropertiesFile(emailPropertiesFilePath)
            messageProperties = appProperties.readPropertiesFile(messagePropertiesFilePath)
        }
    }

    @Throws(IOException::class)
    private fun readPropertiesFile(fileName: String): Properties {
        val logger = LoggerFactory.getLogger(this::class.java)
        var fis: FileInputStream? = null
        var properties = Properties()

        try {
            fis = FileInputStream(fileName)
            properties.load(fis)
        } catch (e: Exception) {
            logger.error("Failed to read $fileName")
            e.printStackTrace()
        } finally {
            fis!!.close()
        }
        logger.info("Successfully read $fileName")
        return properties
    }
}