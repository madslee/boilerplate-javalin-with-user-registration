# Javalin Boilerplate with user authentication

*Disclaimer: Demonstration purposes only, must not be used in production in its present state*

Small Javalin app with user authentication using Koin for dependency injection, Jetbrains Exposed ORM and HikariCP for managing database connections.

